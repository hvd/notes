# Writing
Bad / good examples:
> 𝗫 An understanding of the causal factors involved in excessive drinking by students could lead to more effective treatment.

> ✓ We could more efficiently treat students who drink excessively if we understood why they do so.

## Actions and Characters
A sentence is clear when its **important actions are in verbs**, and **main characters are subjects**. You can derive nouns from verbs or adjectives, or vice-versa. Reassemble using conjunctions such as if, although, because, when, how, why.
> 𝗫 Governmental intervention in fast-changing technologies has led to the distortion of market evolution and interference in new product development

> ✓ When a *government* **intervenes** in fast-changing technologies, it **distorts** how markets **evolve** and **interferes** with their ability to **develop** new products.

Tip: When the character is not clear, use the word “you” as often as you can.

### Active & Passive
**The active voice is recommended** as it’s more direct and easier to read. But the passive voice is helpful in some exceptions:
1. You don’t know who did the action, or readers don’t care:
> ✓ Those who are found guilty can be fined.
2. Exchange the order in which you write the information. (see example in next section)
3. Focus your readers’ attention on a particular character.

## Cohesion and Coherence
Begin sentences with known facts, familiar information and give new, unfamiliar information towards the end of the sentence.
> 𝗫 The weight given to industrial competitiveness as opposed to the value we attach to the liberal arts (new information) will determine (active verb) our decision (familiar information)

> ✓ Our decision (familiar information) will be determined (passive verb) by the weight we give to industrial competitiveness as opposed to the value we attach to liberal arts (new information)

When writing paragraphs, the last few words of one sentence should set up information that appears in the first few words of the next.
> 𝗫 Questions have been raised by scientists studying black holes in space. **The collapse of a dead star into a point perhaps no larger than a marble creates a black hole.**

> ✓ Questions have been raised by scientists studying black holes in space. **A black hole is created by the collapse of a dead star into a point perhaps no larger than a marble.**

Tip: try putting words like this, these, that, another, such and so on, at the beginning of a sentence.

To have all the sentences in a piece of writing add up to a larger whole, your topics should be short, concrete. Together, your subjects should name the topics that tell your readers what a passage as a whole is about.

## Emphasis
Begin sentences with what they are “about”; end with words that should receive a special emphasis.

Which company would you invest in?
> ✓ Although the company’s sales remain strong, **its stock price has slipped**.

> ✓ Although the company’s stock price has slipped, **its sales remain strong**.

Put the context at the beginning, to avoid losing the emphasis of the end of a sentence.
> 𝗫 Climate change could raise sea levels to a point where much of the world’s low-lying coastal areas would disappear, **according to most atmospheric scientists**.

> ✓ According to most atmospheric scientists, climate change could raise sea levels to a point where much of the world’s low-lying coastal areas **would disappear**.

Put key words (your topics) in the first sentence of a paragraph, at the end of this sentence. It will emphasize the key concept that will be repeated and developed.

## Motivation
To motivate the reader, you should address **why your topic is important in the introduction**. A good introduction structure contains: a shared context, a problem, the solution.

A shared context should remind the readers of what they know, have experienced or readily accept. The problem should be a pain point.

## Concision
Shorter is better.
> 𝗫 In my opinion, it is necessary that we should not ignore the opportunity to think over each and every suggestion offered.

> ✓ We should consider each suggestion

Delete meaningless words: *actually, really, various, kind of, generally, various, certain*, so forth. Delete words that repeat the meaning, or implied by other words. Change negatives to affirmatives: not many → few, not careful → careless, not include →omit, not allow → prevent, so forth.

Delete adjectives and adverbs. You don’t need them to strengthen a sentence; **short sentences are strong**.
> 𝗫 Surely, I will really do it.

> ✓ I will do it.

Note: When most readers read a sentence that begins with something like obviously, it is clear that, undoubtedly…; they reflexively think the opposite.

## Shape
For long sentences, start with your point.
> 𝗫 High-deductible health plans and Health Savings Accounts into which workers and their employers make tax-deductive deposits result in **workers taking more responsibility for their health care**.

> ✓ **Workers take more responsibility for their health care** when they adopt high-deductible insurance plans and Health Savings Accounts into which they and their employers deposit tax-deductible contributions.

Avoid long subjects, by revising them into an introductory subordinate clause.

## Coordination
Order coordination parts (... and … and …) **from shorter to longer**.
> 𝗫 We should analyse connections between those subjective values that reflect our deepest ethical choices *and* objective research.

> ✓ We should analyse connections between objective research *and* those subjective values that reflect our deepest ethical choices.

Read your prose out loud. What makes a sentence most graceful is balance and symmetry among its parts; one echoing another in sound, rhythm, structure and meaning.

Tips: to balance parts of a sentence, balance with not only … but also …, use weighty words and the end.

## Punctuation
### Semicolons
1. When you have two or more sentences which you wish the reader to understand as parts of a single thought.
> Training is everything. The peach was once a bitter almond; cauliflower is nothing but cabbage with a college education.
2. When sentence is so long, or so complex, that the reader would welcome a mental rest station along the way.
> With all the world to choose from, they invariably select subjects closest to their inner feelings; and when they choose subjects seemingly alien to them, they invariably alter them to correspond to their personal condition, even though what emerges may seem, to the uninitiated, remote and unrelated.
3. When two or more related sentences are parallel in structure and thus invite pairing to set off either contrast or likeness.
> Some would create a sling scale making long kicks worth more point than short ones; other would return the ball to the line scrimmage instead of the defenders' 20-yard line after missed kicks, making it riskier to attempt long ones.
4. When you wish to separate cleanly the various item in a series, particularly when commas occur within one ore more members of the series; also when you wish to separate the items into classes.
> Among its 35 or so working members are Ivan Allen Jr., the former mayor of Atlanta, and William Baker, the president of Bell Labs; Mary Wells Lawrence, of her own advertising agency, and Clare Boothe Luce; Daniel Patrick Moynihan, now ambassador to India.

### Commas
Insert a comma wherever there is a light natural pause.

### Parentheses
Parentheses give you a way of muscling into a sentence a piece of incidental information which you can't fit in grammatically, or which you don't want to bother to fit in grammatically, or which you want to de-emphasize because it amounts to a little footnote. Use them sparingly; They quickly become an eyesore, and no reader enjoys being repeatedly whispered to.

### Dashes
1. Mark an interruption or break in thought.
> Life without romance - well, you might as well be in prison or a slug under the earth.
2. Serve as a conversational colon or light bridge.
> That's the worst of facts - they do cramp a fellow's style.
3. Isolate a phrase - usually a concluding one - for emphasis or humorous effect.
> The Dow Jones industrial average plummeted nearly 52 points in a week - the worst break in years.
4. Insert an important parenthetical explanation, qualification, or amplification.
IQ tests - and the academic establishment built upon the assumptions about the supreme value of conceptual intelligence - do not measure right-brain intelligence, much less other vital performance factors as emotional understanding and patience.
5. Mark a gathering up of several ideas, often a series of subjects.
> The art of the surprise witness, the withering cross-examination, the sudden objection phrased in arcane formulas - all seems to bespeak a profession based on elaborate training and requiring consummate skill.
6. A long column of type can look forbiddingly heavy and black unless it's relieved by occasional dashes, which lighten it up and offer variety.
7. When nothing but commas are used to set off modifying phrases, the commas tend to pile up quickly and clog one's sentences. The dash helps relieve the congestion, not to mention the monotony.
8. Dashes are useful as an italics-substitute for calling attentions to key word or phrase.
9. Dashes are also useful as a substitute for colons.
10. It's a good idea to rest the dash for three or more sentences after each use. If you overuse it, it will lose its effect.
11. Never use a comma alongside a dash, even it the syntax seems to require it.

### Colons
1. The colon joins related thoughts. However, it is used only when the first thought acts as an introduction or prelude to the second. The colon is roughly equivalent to _that is_ or _namely_.
> Girls have an unfair advantage over men: if they can't get what they want by playing smart, they can get it by playing dumb.
2. Introduce a list or series.
3. Don't use more than one colon per sentence and don't allow a semicolon to follow a colon.

### Quotation marks
Set off a word or phrase by quotation marks when making reference to it or when using it in a special sense (e.g., technically, humorously, ironically). If you ever use a cliche, never - repeat never - put it in quotation marks.

### Abbreviations
Avoid using them freely in the body of your text - you'll risk looking either pedantic or lazy.

## References
Style: Lessons in Clarity and Grace 11th Edition - Joseph M. Williams, Joseph Bizup

Writing with Style Conversations on the Art of Writing - John R. Trimble

http://www.plainenglish.co.uk/how-to-write-in-plain-english.html

https://www.hemingwayapp.com/

https://developers.google.com/tech-writing

https://developers.google.com/style
