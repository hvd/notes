# Misc
# Brew
Neovim nightly
```bash
brew upgrade --fetch-HEAD neovim
```
List all packages installed by user
```bash
brew bundle dump --file -
brew list --cask -l1
```

## Snap
Neovim nightly
```bash
sudo snap install --edge nvim --classic
sudo snap refresh nvim
```

## Ubuntu list all manually installed packages
```bash
comm -23 <(apt-mark showmanual | sort -u) <(gzip -dc /var/log/installer/initial-status.gz | sed -n 's/^Package: //p' | sort -u)
```

## Chocolatey
```
choco list --local-only
choco search <name>
choco install <package_name>
choco upgrade all
```

## Stow
Show and not actually do anything
```bash
stow -n -v <stow_package>
```
Stow to a location
```bash
stow -t <location_path> <stow_package>
```
Delete
```bash
stow -D -target <location_path> <stow_package>
```

## Git
clone just master and selective refs
```bash
git init <repo>
cd <repo>
git remote add -t master origin git@git.musta.ch:<name>/<repo>.git
git config --add remote.origin.fetch '+refs/heads/me--*:refs/remotes/origin/me--*'
git fetch
git checkout -b master origin/master
```
fetch just the remote branch
```bash
git fetch <remote> <branch_name>:<local_name>
git checkout <local_name>
```
reset local master to remote master
```bash
git fetch origin
git reset --hard origin/master
```
worktree, new worktree dir doesn't need to exist
```bash
git worktree add <new-worktree-dir-path> <existing_branch>
```
clean everything that is not tracked by git recursively ignoring ignore file, back up any config files before proceed
```bash
git clean -fdx
```
amend commit message (also work with add changes if done after adding the new changes)
```bash
git commit --amend -m "New commit message."
```
remove a large file from commit history
```bash
git filter-branch --tree-filter 'rm -f blob.txt' HEAD
git update-ref -d refs/original/refs/heads/master
git reflog expire --expire=now --all
git gc --prune=now
git push -ff
```
for huge repo, turn on filesystem monitoring
```
git config core.fsmonitor true
git config core.untrackedcache true
```
git history of a specific file including file rename
```bash
git log --follow -- <path>
```
of specific lines
```bash
git log -L<start>,<end>:<path>
```
search commits
```bash
git log --all -i --grep='blah'
```
see diff of a file at a particular commit (between it and its parent)
```bash
git difftool <commit>^! -- <path>
```
see stats of a commit
```bash
git diff --stat <commit>^!
```
see all config options
```bash
man git-config
```
to init existing submodule using a filter
```bash
git submodule update --init --filter=blob:none
```
to add submodule using a filter
```bash
git clone --filter=blob:none sub/mod/url [submodpath]
git submodule add sub/mod/url [submodpath] # doesn't download
git submodule absorbgitdirs
```
to remove a submodule
```bash
git rm <path-to-submodule>
```
to completely wipe out all git info of the submodule
```bash
rm -rf .git/modules/<path-to-submodule>
git config --remove-section submodule.<path-to-submodule>
```
to update all submodules
```bash
git submodule update --remote --recursive
```

## Test if terminal has true color support:
```bash
awk 'BEGIN{
    s="/\\/\\/\\/\\/\\"; s=s s s s s s s s;
    for (colnum = 0; colnum<77; colnum++) {
        r = 255-(colnum*255/76);
        g = (colnum*510/76);
        b = (colnum*255/76);
        if (g>255) g = 510-g;
        printf "\033[48;2;%d;%d;%dm", r,g,b;
        printf "\033[38;2;%d;%d;%dm", 255-r,255-g,255-b;
        printf "%s\033[0m", substr(s,colnum+1,1);
    }
    printf "\n";
}'
```

## Windows /etc/hosts
Notepad run as administrator
```
C:\Windows\System32\drivers\etc\hosts
```

## Ublock origin filter
- `old.reddit.com*` to block just that main url
- `example.com` to block all urls matching with domains
- `example.com/foo/^$document` to block a particular url

## Vim
To make `cdo` operation on many files go faster or temporary change `eventignore` to `all` also use `cfdo` instead to batch all substitutions in a single file to just one write.
```vim
:noautocmd cdo %s/toBeReplaced/string/g
```
To make a function dot repeatable
```vim
:h operatorfunc
```
run a shell cmd (non-interactive)
```vim
:!
:system()
```
Run a shell cmd asynchronously (non-interactive)
```vim
jobstart()
```

## Disable autoplay
### Firefox
```
about::config
media.autoplay.enabled
```

### Chrome
```
chrome://flags
autoplay
```
