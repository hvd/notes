# Engineering
#### Table of contents
- [System design](#system-design)
- [Testing](#testing)
- [Monitoring](#monitoring)
- [Debug](#debug)
- [CI/CD](#ci-cd)
- [Design document](#design-document)

## System design
### Principles
Remember we humans have limited memory. We can only keep a handful of things in our head at a time so **keep it simple**, draw and write things down:
- Composition > inheritance - **Avoid inheritance if you can**.
- Simple interface covers the common use cases, yet still provides significant functionality (deep module).
- A module that hides a lot of information will tend to increase functionality while reducing its interface.
- Try not to be influenced by runtime operation order when creating module.
- General purpose modules are deeper, minimize specialization code (exception code is a type of specialization code).
- To make code obvious: follow convention, good name/comment, good abstraction and eliminating special cases.
- Don't optimize until you've understood the bottlenecks.
- Perfect is the enemy of good.

Don't buy into the followings too much.
- Single responsibility - aim for high cohesion (all related functionalities in one unit) and low coupling (low number of dependents)
- Open for extension but closed for modification - create classes such that new behaviors are added via composition or inheritance without editing existing classes.
- Liskov substitution - every subclass or derived class should be substitutable for their parent or base class.
- Interface segregation - clients should not be forced to depend upon interfaces that they do not use, i.e., no fat interface - splits up into smaller interfaces.
- Dependency inversion - classes should depend on abstractions, interfaces or behavior and not concrete implementations. Classes should not be responsible for creating instances for their dependencies. Rather, such **dependencies should be injected** into them.

### Imperative shell with functional core
Model of designing applications around domain logic to isolate it from external factors. The domain logic is specified in a business core which we'll call the inside part, the rest are called the outside parts. Access to domain logic from the outside is available through ports and adapters. Application are divided into three layers:
1. application (outside) - where the user or any other program interacts. Should contain things like user interfaces, RESTful controllers, and JSON serialization libraries. It includes anything that exposes entry to the application and orchestrates the execution of domain logic.
2. domain (inside) - keep the code that touches and implements business logic. Isolated from both the application and infrastructure layer. Contains interfaces that define API to communicate with the outside parts.
3. infrastructure (outside) - configurations or anything that the application needs to work.

### Resources
A Philosophy of Software Design by John Ousterhout

https://www.baeldung.com/hexagonal-architecture-ddd-spring

https://fsharpforfunandprofit.com/ddd/

https://github.com/fpereiro/backendlore

## Testing
- Avoid mocks, use fake instead.
- Test for behavior (return value and side effects) not implementation (right dependencies are called in the right order with the right values)
- Don't mock internals, privates, or adapters; There shouldn't be any new tests when refactored.
- In general, break your testings down to code branching, each test should follow the Given (input), When (action), Then (output) or Arrange, Assign, Assert pattern

## Debug
- Search and read debugging work already done
- Use debugger; if debugger is not viable or concurrency involves, debug logging and make them stood out and easily searched
- git bisect to identify offending commit

## Monitoring
- Start from the perspective of the users, the api that they use
- Alerts must be actionable and not flaky
- Alerts have troubleshooting instructions

## CI CD
Iteration speed is momentum. The faster one can write and deploy code the more motivated one is going to do more and the faster products are delivered. Focus on:
- compile time
- source control
- one click deployment
- phased rollout
- monitoring
- roll back
- static typing

## Design document
### Topics
- stakeholders, requirements/goals, non-goals
- high level architecture, key data structures, code lay out, data flows
- apis, sequence diagrams of common use cases
- trade offs / here be dragons, the whys
- security
- privacy
- testing
- estimates / timelines
- performance (rate limiting, caching, etc.)
- external dependencies
