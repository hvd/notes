# Rust
## Pointer types
### References
Two flavors:
- `&T` an immutable, shared reference. Can have many shared reference to a given value at a time, but they are read-only.
- `&mut T` a mutable, exclusive reference. Can read and modify the value it points to. As long as the reference exits, can not have any other references of any kind to that value.

Rust references are never null in safe rust.

### Boxes
`Box<T>` is a pointer to value of type `T` stored on the heap. `Box::new` simplest way to allocate a value in the heap.

## Arrays, vectors, and slices
- `[T; N]` array of `N` values, each of type `T`. N is a constant determined at compile time and is part of the type, can't append or shrink.
- `Vec<T>` dynamically allocated, growable, and live on the heap.
- `&[T]` and `&mut [T]` are shared slice of `T`s and mutable slice of `T`s. Slice is a pointer to the first element, together with a count of the number of elements that can be access starting from that element.

## String
```rust
let noodles = "noodles".to_string();
let oodles = &noodles[1..];
let poodles = ":(";
```
- `noodles` is a `String` that has a resizable buffer holding UTF-8 text. The buffer is allocated on the heap, so it can resize its buffer as needed or requested. `String` is analogous to `Vec<T>`.
- `&str` can refer to any slice of any string, whether is a string literal or a `String`. `oodles` is a reference to a run of UTF-8 text owned by someone else. `poodles` is a string literal `&str` that refers to preallocated text, typically stored in read-only memory. `&str` is much like `&[T]` a fat pointer to some data and cannot be modified. `&mut str` does exist, but it is not useful since almost any operation on UTF-8 can change its overall byte length, and a slice cannot reallocate its referent.
- `PathBuf` and `&Path` is for filenames.
- `OsString` and `&OsStr` for environment variable names and command-line arguments.
- `CString` and `&CStr` for working with C libraries.

## Ownership
```rust
struct Person { name: String, birth: i32 }
let mut composers = Vec::new();
composers.push(Person { name: "name".to_string(), birth: 1525 });
```
Vector `composers` owns all of its elements. Each element of `composers` owns a string, which owns its text. The owners and their owned values form trees. At the ultimate root of each tree is a variable; when that variable goes out of scope, the entire tree goes with it. Every value has a single owner that determines its lifetime. When the owner is dropped, the owned value is dropped too. In rust:
- You can move values from one owner to another.
- Simple types like integers, floating-point numbers, and characters are excused from the ownership rules. These are called `Copy` types.
- The standard library provides the reference-counted pointer types `Rc` and `Arc`, which allow values to have multiple owners, under some restrictions: `Arc` (atomic reference count) is thread safe whereas `Rc` is not. An `Rc<T>` value is a pointer to a heap-allocated `T` that has a reference count affixed to it. Cloning an `Rc<T>` value does not copy the `T`; instead, it simply creates another pointer to it and increments the reference count.
- You can borrow a reference to a value; references are non-owning pointers, with limited lifetimes.

In terms of moving semantic:
- The call `Vec::new()` constructs a new vector and returns, not a pointer to the vector, but the vector itself: its ownership moves fro `Vec::new` to the variable `composers`.
- The `name` field of the new `Person` structure is initialized with the return value of `to_string`. The structure takes ownership of the string.
- The entire `Person` structure, not a pointer to it, is passed to the vector's `push` method, which moves it onto the end of the structure. The vector takes ownership of the `Person` and thus becomes the indirect owner of the `name` string as well.

## Tools
### Cargo
