# Linear Combinations, Span, Basis Vectors
- Conventionally, `i` and `j` are the basis vectors in 2D with point (0, 0) at
the origin.
- `ai + bj` is the linear combination where `a` and `b` are scalars
(they scale the basis vector `i` and `j`).
- The set of all possible vectors that one can reach with the linear combinations of a given pair is called span.
- When one or multiple vectors do not add to the span they are said to be
linearly dependent.

# Linear Transformations and Matrices
- Linear transformations are ways to move around space such that grid lines
remain parallel and evenly spaced and such that the origin remains fixed.
- These transformations can be described by a handful of numbers - the
coordinates where each basis vector lands. Matrices give us a language to
describe these transformations where the columns represent those coordinates.
- For 2D linear transformation where its vectors are linearly dependent, the
linear transformation squishes all 2D space onto the line where those two
vectors sit (1D span of those two linear dependent vectors).
- Matrix multiplication then is just composition of linear transformations.

# Determinant
- for 2D it answers how much are areas scaled? and for 3D how much volume?
- Negative determinant indicates orientation flipping.
- for 3D a determinant of 0 would mean the transformation squish all of space
into either a flat plane, a line or a point, which mean the vectors of the
linear transformation are linearly dependent.

# Inverse Matrices, Column Space and Null Space
- System of equations can be translated in geometric interpretation as finding the unknown vector `x` that would land on vector `y` after transformation `A`.
- As long as transformation A does not squish all of space onto a lower
dimension meaning its determinant is nonzero there will be an inverse
transformation. To find vector `x` just multiply that inverse matrix
transformation with vector `y`.
- Solutions can still exists when determinant is zero if the vector `y` is on
the line as in the 2D case and on either a line or in a plane in the 3D
case.
- Rank means the dimensions in the output of a transformation or in the column space. For a 2x2 matrix, rank 2 is the best
this transformation can be or full rank.
- Column space is the set of all possible outputs for a matrix or is the span
of the columns of a matrix. The zero vector is always in the column space.
- The set of vectors that lands on the origin is called the null space or kernel.

# Dot Products and Cross Products
- Dot product of two vectors is the scaling of one vector by a factor equal to
the length of the projection of the other vector onto that aforementioned
vector or simply the measure of similarity of vectors.
- The magnitude of the cross product of two vectors is the area of the
parallelogram with the two vectors as adjacent sides, and the direction is that
perpendicular to both vectors.

# Change of Basis
Take the coordinates of the vectors that you want to use as the new basis, make them the columns of a matrix (change of basis matrix). Sandwich the original matrix putting the change of basis matrix on its right and the inverse on its left, the result will be a matrix representing that same transformation but from a perspective of the new basis' coordinate system.


# Eigenvectors and Eigenvalues
- Eigenvectors of a linear transformations are vectors that remains on their
own respective vectors (stay on their spans) after the transformation.
- The associated factors by which they are stretched are called eigenvalues.
- For 3D, these are useful to think about as axes of rotation of the linear
transformation.
- One can see that to find these eigenvectors and their associated eigenvalues,
one need to solve `Av = \v` or `(A - \I)v = 0` where v is the nonzero eigenvectors or simplify
further to solving `det(A - \I) = 0`. In another words, tweak `\` so that space
is squished into lower dimensions.
- Some linear transformations don't have eigenvectors.
- An eigenvalue can have more than one eigenvectors.
- Basis vectors that are also eigenvectors (eigenbasis) form a diagonal matrix, which can be used to easily compute
power of a matrix. Change the matrix to eigenbasis, compute the power and then change it back. Not all matrices can become diagonal.

# Abstract Vector Spaces
- Linear algebra can be abstracted and can be applied to many other vectory things in addition to the usual application to arrows in space.

# Resources
- 3blue1brown -- Essence of linear algebra
