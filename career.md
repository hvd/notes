# Career
## Resume
- Keep it simple and clean. No header, footer or photo.
- Consistent branding -- one email, one number, one online profile. Don't list github if not active.
- Summary and skills up top.
> Meticulous Financial Analyst who undertakes complex assignments, meets tight deadlines and delivers superior performance.
> Possesses practical knowledge in corporate finance and financial markets.
> Operates with a strong sense of urgency and thrives in a fast-paced setting.
- Reverse chronological order for professional experience.
- Have company description.
> Company ABC * New York, NY * 2010 - Present

> One of the largest global pharmaceutical companies, producing a portfolio of products and medicines that support wellness and prevention, as well as treatment and cures for diseases across a broad range of therapeutic areas.

> Financial Analyst
- Use bullet points.
- Every 3 sentences, use at least 1 number to quantify your concrete impact.
> Generated approximately $452,000 in annual savings by employing a new procedure which streamlined the business's vendor relationships
- Don't mention more than once or twice that you're a "team player".
- Result and cause format.
> Created and launched a service that collects product opinions and recommendations from Twitter. The service finds related tweets, removes spam, analyzes sentiment and creates a structured database of everything that was said about particular products.

instead of

> Designed software application including: data modeling, software architecture design, software-hardware integration, user interface design, and database management.
- Incorporate 1-2 leadership-oriented words every 5 sentences (communicated, coordinated, leadership, managed, organization etc.)
- Don't use personal pronouns.
- Add 15-20 skills, buzzwords, acronyms.
- Describe job achievements with different action verbs
> Developed a world positive, high-impact student loan product.

instead of

> After 100+ customer interviews, the world-positive, high-impact student loan product was developed by me.
- Education at bottom. Don't list graduation date if older than 35.
- No "references upon request".
- Use word cloud to figure out what words stand out.
- Apply on Mondays, in the first 4 days, between 6AM and 10AM.

## Prep
1. Get good at coding all the standard classes of problem and solution, should not take longer than 20 minutes.
2. Then it's a matter of increase your database of problems; at this point complete code is not necessary.

## During technical interview
- Repeat, clarify the question, identify constraints, and verify valid inputs assumption
- Work through concrete example
- Start with a brute force approach and refine to the optimal answer; give time and space complexity; discuss trade-offs
- Start coding once the interviewer have agreed on an approach; focus on the main parts of the algorithm first
- Review, walk through the code with test inputs, and pretend you are the debugger -- write out and update variable table

## Behavior interview
- Be genuine
- Don't afraid to use 'I' instead of 'We'

## Questions
### For Company
- What does dev cycle look like? Waterfall/sprint/agile?
- Are rushes to deadlines common? Or is there flexibility?
- How are decisions made in your the team?
- How many meetings per week?
- Do you feel your work environment helps you do deep work?
- If there is one thing you could get the company to stop doing today, what is it?
- On that note you're in tech, there are numerous opportunities everywhere, what are the top three things that keep you here?
- What's the job you want after this one? Why? How is the company helping you get there?
- What do you and your manager discuss in your 1:1s? What do you and your reports discuss in your 1:1s?
- Name a mistake you made in the past year. What did you learn from it? How did your team and/or manager respond?
- Same question as above but name a success.
- Performance management.
- On-call.
- Remote work.

### For yourself
- Why do you want this job?
- What's a tough problem you've solved?
- Biggest challenges faced?
- Best/worst designs seen?
- Ideas for improving an existing product.
- How do you work best, as an individual and as part of a team?
- Which of your skills or experiences would be assets in the role and why?
- What did you most enjoy at [job x / project y]?
- What was the biggest challenge you faced at [job x / project y]?
- What was the hardest bug you faced at [job x / project y]?
- What did you learn at [job x / project y]?
- What would you have done better at [job x / project y]?

## Resources


https://github.com/yangshun/tech-interview-handbook

https://github.com/donnemartin/system-design-primer

https://seanprashad.com/leetcode-patterns/

https://leetfree.com/

https://github.com/labuladong/fucking-algorithm/tree/english

https://whoishiring.io

https://www.levels.fyi


