# Haskell
#### Table of contents
- [Types](#types)
- [Typeclasses](#typeclasses)
- [List](#list)
- [Algebraic datatypes](#algebraic-datatypes)
- [Monoid](#monoid)
- [Functor](#functor)
- [Applicative](#applicative)
- [Monad](#monad)
- [Foldable](#foldable)
- [Traversable](#traversable)
- [Monad transformers](#monad-transformers)
- [Nonstrictness](#nonstrictness)
- [IO](#io)
- [Resources](#resources)

## Types
### Datatypes
```haskell
type Name = String
data Pet = Cat | Dog Name
```
- `Name` is an alias for String
- `Pet` is a datatype and operates as a type constructor
- `Cat` and `Dog` are data constructors

Data declarations define new datatypes in Haskell. It always create a new type constructor, but may or may not create new data constructors. Data constructor provides a mean of creating values that inhabit a given type. Type constructor is used to denote type. A type alias a way to refer to a type constructor or type constant by an alternate name usually for brevity.

```haskell
isEqual :: Eq a => a -> a -> Bool
isEqual x y = x == y
```
- `Eq` is a type class.
- `a` is polymorphic type variable but in this case constrained to `Eq` type class.

A typeclass is a set of operations defined with respect to a polymorphic type. Polymorphism in Haskell means being able to write code in terms of values which maybe one of several, or any type. Arity is the number of arguments a function accepts. In Haskell, due to currying all functions are 1-arity. Accepting multiple arguments is handled by nesting functions.

### Currying
`(->)` is the type constructor for functions. It is an infix right associative operator and has no data constructors.
```haskell
map :: (a -> b) -> [a] -> [b]
-- associates into
map :: (a -> b) -> ([a] -> [b])
let curry f a b = f(a, b)
let uncurry f (a, b) = f a b
```
Note: the association here (grouping into parentheses) is not to control precedence but to only group the parameters into argument and result. There can only be one argument and one result per arrow. This nesting of multiple functions, each accepting one argument and returning one result, to allow the illusion of multiple-parameter functions is called currying. Currying also leads to partial application - when apply only some of a function's arguments.
```haskell
let x = 5
let y = (2^)
let z = (^2)
y x
32
z x
25
```
`y` and `z` are both partially applied and the above demonstrates sectioning (choosing which argument to partially applied for) in partial application in this case for infix operator `^`.

## Typeclasses
### Instance
An instance is the definition of how a typeclass should work for a given type. Instances are unique for a given combination of typeclass and type.
```haskell
data DayOfWeek = Mon | Tue | Wed | Thu | Fri | Sat | Sun
data Date = Date DayOfWeek Int
instance Eq DayOfWeek where
  (==) Mon Mon = True
  (==) Tue Tue = True
  (==) Wed Wed = True
  (==) Thu Thu = True
  (==) Fri Fri = True
  (==) Sat Sat = True
  (==) Sun Sun = True
  (==) _   _   = False
```
In the above, if the last unconditional case is left out, the instance then is a partial function - one that doesn't handle all the possible cases. This would result in exception at runtime; turn on `-Wall` flag to detect this.

### Inheritance
```haskell
class Num a => Fractional a where
(/) :: a -> a -> a
recip :: a -> a
fromRational :: Rational -> a
```
`Fractional` inherits from `Num`. If one wants to write an instance of `Fractional` for some `a`, that type `a`, must already have an instance of `Num`.

## List
```haskell
-- list datatype in Haskell is defined using cons operator (:)
data [] a = [] | a : [a]
1 : (2 : (3 : []))

-- diy
data List a = Nil | Cons a (List a)
Cons 1 (Cons 2 (Cons 3 Nil))
```
Representation of list in Haskell
```
  :
 / \
1   :
   / \
  2   :
     / \
    3  []
```
`:` is called the spine - connective structure that ties the collection of values together. Coupled with nonstrict evaluation in Haskell, Cons cells (: nodes) can be evaluate independently of what they contain. It is possible to evaluate only the spine or part of it without evaluating individual values. Evaluation of the list proceeds down the spine while constructing the list (when that is necessary) proceeds up the spine. In the example above, arguments 1 and a new cons cell is evaluated then proceed downward to the 3 and empty list. When the list is built, it proceeds from the bottom of the list up the spine, first putting the 3 into the empty list, then adding the 2 to the front of that list and so on. A common mantra for performance sensitive code in Haskell is, **lazy in the spine, strict in the leaves**.
```haskell
let x = [1] ++ undefined ++ [3]
-- Undefined exception
x

let y = [1, undefined, 3]
-- length will return 3 despite bottom value undefined
length y
```
Values in Haskell get reduced to weak head normal form by default. Normal form means an expression is fully evaluated. Weak head normal form means an expression is only evaluated as far as is necessary to reach a data constructor or lambda awaiting an argument. WHNF is a superset of NF.

## Algebraic datatypes
Type is a enumeration of constructors that have zero or more arguments. There are two kinds of constructors in Haskell: type constructors and data constructors. Type constructors are used only at the type level, in type signatures and typeclass declarations and instances. They are static and resolve at compile type. Data constructors construct values at term level, values one can interact with at runtime. Type and data constructors that take no arguments are constants. When a constructor takes an argument, then it's like a function in at least one sense - it mus be applied to become a concrete type or value.

Kinds are the types of types. Concrete type is represented as `*`. If it is `* -> *`, then it's still waiting to be applied. The kind `* -> * -> *` must be applied twice before it will be a real type also known as a higher-kinded type.

`newtype` is a keyword to mark type that can only ever have a single unary data constructor. A `newtype` cannot be a product type, sum type, or contain nullary constructors. It has no runtime overhead because it reuses the representation of the type it contains. The difference between newtype and the type it contains is gone by the time the compiler generates the code.
```haskell
{- # LANGUAGE GeneralizedNewtypeDeriving #-}
class TooMany a where
  tooMany :: a -> Bool

instance TooMany Int where
  tooMany n = n > 42

-- can define typeclass instances for newtypes that differ from the instances for their underlying type
-- in this case can be automatically derived using a pragma
newtype Goats = Goats Int deriving (Eq, Show, TooMany)
```
Record (product type) syntax:
```haskell
data Person = Person {name :: String, age :: Int} deriving (Eq, Show)
let papu = Person "Papu" 5
name papu
newtype Combine a b = Combine { unCombine :: (a -> b) }
```

Kind * is the kind of all standard lifted types, while types that have kind # are unlifted. A lifted type is any that can be inhabited by bottom and unlifted type cannot be inhabited by bottom. Lifted types are represented by a pointer while unlifted are often native machines types and raw pointers. Newtype itself cannot be inhabited by bottom, only the thing it contains can be, so newtypes are unlifted.

## Monoid
Monoid is a binary associative operation with an identity.
```haskell
class Monoid m where
  mempty :: m
  mappend :: m -> m -> m -- <> infix operator
  mconcat :: [m] -> m
  -- defining mconcat is optional, since it has the following default:
  mconcat = foldr mappend mempty
```
In layman's terms, binary functions that let one joins things together in accordance with the laws of associativity and also have an identity
value are monoids. List forms a monoid under concatenation.
```haskell
instance Monoid [a] where
  mempty  = []
  mappend = (++)
```
`Integers` forms a monoid under both summation and multiplication so there is no single default monoid for it. Use `newtype` to separate the different monoidal behaviors:
- signal intent using newtype as a wrapper for the underlying type
- improve type safety - avoid mixing up many values of the same representation
- add different typeclass instances to a type that is otherwise unchanged representationally

Semigroup is a monoid  without an identity value. NonEmpty list is an example
of semigroup.

## Functor
Functor is a way to apply function to the value that is inside some structure (lifting functions over structure) and leave the structure alone.
```haskell
class Functor f where
  fmap :: (a -> b) -> f a -> f b  -- infix operator <$>
```
Note `f a` is a functor `f` that takes a type argument `a`. That is, the `f` is a type that has an instance of the `Functor` typeclass.

An example functor for `Maybe`
```haskell
instance Functor Maybe where
  fmap f Nothing  = Nothing
  fmap f (Just x) = Just (f x)
```

Functor laws:
```haskell
fmap id == id                   -- Identity
fmap (f . g) = fmap f . fmap g  -- Composition
```
`Functor` is an example of higher-kinded polymorphism because the kind of the `f` parameter to Functor must be `* -> *`. As a result, for generic product and sum types, `fmap` only transforms the second argument.

If we want to transform only the structure and leave type argument, we need natural transformations:
```haskell
-- want nat :: (f -> g) -> f a -> g a but f an g are higher kinded types; need them to be kind *
{-# LANGUAGE RankNTypes #-}
type Nat f g = forall a . f a -> g a[
```
Syntactically, the above lets us avoid talking about `a` or contents of `f` and `g`.

## Applicative
Applicative is a monoidal functor, which allows for function application lifted over structure but with the function is also embedded in some structure. Because the function and the value it's being applied to both have structure, we have to smash those structures together
```haskell
class Functor f => Applicative f where
  pure  :: a -> f a
  (<*>) :: f (a -> b) -> f a -> f b  -- called star fighter or ap short for apply
```
Applicative has `Monoid` for structure and function application for values plus `Functor fmap` to be able to map over the `f` to begin with (bolting a `Monoid` onto a `Functor`).
```haskell
mappend :: f             f      f
$       :: (a -> b)      a      b
(<*>)   :: f (a -> b) -> f a -> f b
```
Applicative instance of `Maybe`
```haskell
instance Applicative Maybe where
  pure                  = Just
  (Just f) <*> (Just x) = Just (f x)
  _        <*> _        = Nothing
```
`Applicative` instance of tuple
```haskell
-- doesn't need Monoid for the b because we use function application to produce b
-- first value needs to be a Monoid so that we can two values into one value of the same type
instance Monoid a => Applicative ((,), a)

Prelude> ("Woo", (+1)) <*> (" Hoo!", 0)
("Woo Hoo!", 1)
```
Applicative laws:
```haskell
pure id <*> v = v                            -- Identity
pure f <*> pure x = pure (f x)               -- Homomorphism
u <*> pure y = pure ($ y) <*> u              -- Interchange
pure (.) <*> u <*> v <*> w = u <*> (v <*> w) -- Composition
```
- Identity - applying `pure id` does nothing.
- Homomorphism - `pure` preserves function applications.
- Interchange - self explanatory.
- Composition - like plain function composition

## Monad
Monads are applicative functors. What differentiates monad from applicative and functor is monad's ability to alter 'structure' with its `bind` operator. Instead of an ordinary function of type `a` to `b`, one functorially apply a function which produces more structure itself and using join to reduce the nested structure that results.
```haskell
class Applicative m => Monad m where
  (>>=)  :: m a -> (a -> m b) -> m b  -- bind
  (>>) :: m a -> m b -> mb            -- sequencing operator or informally Mr.Pointy
  return :: a -> m a
```
`do` and `<-` are syntatic sugars for sequence and bind respectively.

Monadic laws:
```haskell
m >>= return     =  m                        -- right unit
return x >>= f   =  f x                      -- left unit
(m >>= f) >>= g  =  m >>= (\x -> f x >>= g)  -- associativity
```
`return` doesn't perform any computation, it just collects values. The law of
associativity makes sure that the bind operator only cares about the order of
computations and not their nesting.

Use Kleisli fish (`>=>`) for monad composition.

## Foldable
Class of data structures that can be folded to a summary value.
```haskell
class Foldable t where
  {-# MINIMAL foldMap | foldr #-}
  fold :: Monoid m => t m -> m
  fold = foldMap id

  foldMap :: Monoid m => (a -> m) -> t a -> m
  foldMap f = foldr (mappend . f) mempty
```

## Traversable
A way to traverse a data structure, mapping a function inside a structure while accumulative the applicative contexts along the way. Or anytime one needs to flip two type constructors around, or map something and then flip them around.
```haskell
class (Functor t, Foldable t) => Traversable t where
  {-# MINIMAL traverse | sequenceA #-}
  traverse :: Applicative f => (a -> f b) -> t a -> f (t b) -- flipping two structures around
  traverse f = sequenceA. fmap f

  sequenceA :: Applicative f => t (f a) -> f (t a) -- also flipping two structures
  sequenceA = traverse id
```
Traversable laws:
```haskell
t . traverse f = traverse (t . f) -- naturality - should be able to float a function over the structure into the traversal itself
t . sequenceA = sequenceA . fmap t
traverse Identity = Identity -- identity - a Traversable instance cannot add or inject any structure or effects
sequenceA. fmap Identity = Identity
traverse (Compose . fmap g .f) = Compose . fmap (traverse g) . traverse f -- composition - can collapse sequential traversals into a single traversal
sequenceA. fmap Compose = Compose . fmap sequenceA . sequence A
```
## Monad transformers
Monads are not closed under composition unlike functors and applicatives. In addition, monads in general do not commute. Composing monads let us build computations with multiple effects. Monad transformer is a type constructor that takes a `Monad` as an argument and returns a `Monad` as a result. Do the simplest thing first, it's better to let more structured formulations of programs fall out naturally.

## Nonstrictness
Most expression are only reduced or evaluated when necessary and from the outermost parts of expressions and works inward based on what values are forced. A thunk (placeholder in the underlying graph of the program) is created for each expression. If the thunk is never needed, it never gets reduced then the GC will clear it. If it is evaluated it can be shared between expressions so it does not have to be recomputed. Not all expressions get thunked; GHC will not thunk fully applied data constructors (because they are constant hence safe optimization).
```haskell
-- Evaluation in Haskell is demand drive, can't guarantee that something will ever be evaluated
-- have to create links between nodes in graph of epxressions where forcing one expression will force yet another expression
-- also can use bang pattern ! instead of seq
seq :: a -> b -> b
seq bottom b = bottom
seq _ b = b
```

`Strict|StrictData` extension allows one to avoid putting in pervasive uses of `seq|!` and that's all; it won't suddenly make lazy data structures defined elsewhere behave differently, although it does make functions defined in that module processing lazy data structures behave differently.

## IO
`IO` exists mainly to give us a way to order operations (nested lambdas) and to disable some of the sharing (values that are not dependent on `IO` can still be shared). GHC is free to do a lot of reordering of operations, delaying evaluation, sharing of named values, and duplicating code via inlining. `IO` type turns off most of these abilities. Because values of type `IO a` are not an `a` but a description of how one might get an `a`, using `IO` still preserves referential transparency i.e. given the same arguments, the same `IO` action (or recipe) is still generated.

## Resources
- http://adit.io/
- https://github.com/haskell-perf/checklist
- http://www.jerf.org/iri/post/2958
