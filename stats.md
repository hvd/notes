# Statistical studies
- Sample study - estimate a population parameter
- Observational study - understand how two parameters in a population move together
- Experiment - control vs treatment; participants don't know which group they are in in a blind experiment; administering person doesn't know which group the participant is in for double blind; in matched pairs design, control now becomes treatment and treatment becomes control

# Sample variance, standard deviation, and bias
For sample variance, dividing `n` gives an underestimate biased sample variance. To get unbiased sample variance, we must dvide by `n - 1`.

# Z-score
Z-score is the number of standard deviations from the mean from a particular data point.

# Density curve
Area of under the curve is the percent of the data fall between the range.

# Central limit theorem
Suppose a sample is obtained containing may observations, each observation being randomly generated in a way that does not depend on the values of the other observations, and that the arithmetic mean of the observed values is computed. If this procedure is performed many times, the distribution of the average will be closely approximated by a normal distribution.

# Significance tests
## Null hypothesis
No difference hypothesis

## p-value
Assume the null hypothesis were true what is the probability that we got the result that we do for our sample (`P(x_bar|H_0 true)`). If p-value < significance level then reject null hypothesis. Must set significance level ahead of time.

## Power
`P(rejecting H_0 | H_0 false)` or `P(not making Type II error)`. Increasing significance level or sample size would increase power. Albeit increasing significance level would also increase type I error.

## z vs t statistics
z statistics is for proportions while t is for means. However the value to look up is calculate in the same way, which table z or t to look up depends on the sample size. If sample size is at least 30 then look up in z otherwise look up in t.

