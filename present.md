# Present
## Start
- Do not start with a joke.
- Promise - tell the audience what they will learn by the end of your talk.
- Should be done real quick in no more than 5 minutes.

## During
- Outline the talk.
- Cycle on the subject – make your idea repeated many times in order to be completely clear for everyone.
- Make a “fence” around your idea so that it can be distinguished from someone else’s idea.
- Verbal punctuation – enumerating or ask a question to help audience get back on the talk.

## Place and time
- Best time for having a lecture is 11 AM (not too early and not after lunch).
- The place should be well lit and be chosen according to the amount of listeners.

## Tools
- Consider using a blackboard. 
- Props – use them in order to make your ideas visual.
- Keep slides minimal - humans have only one language processing center so have few texts and one or two pictures.
- Prepare two versions of your slides, one for live presentation and another for serving as handouts
- Don't use pointers - use arrows in your slides.

## Package your ideas with 5 star method
- Symbol to represent your idea 
- Slogan 
- Surprise
- Salient idea that sticks out
- Story (how it works, how you did it)

## End
- Don’t put collaborators at the end, do that at the beginning.
- Just have a contribution slide – to sum up your contributions.
- Jokes are okay to end.
- Don't thank the audiences because it's weak and trite, do a benediction instead (You enjoyed giving the talk and would love to come back again.)

## Resources
https://ocw.mit.edu/resources/res-tll-005-how-to-speak-january-iap-2018/