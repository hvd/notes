# Derivative
Instantaneous rate of change is an oxymoron. It's more of best constant approximation of change around a point as you nudge it just a tiny bit (e.g. `dt` approaches zero, slope of the line tangent to the point).

## Geometric Interpretation
- Think function product as area (e.g `x^2` as a square and how the area changes as you nudge `x` a bit)
- `1/x` how much area of the rectangle changes so that the area remains 1 as you nudge x
- `sin(x)` think unit circle and zoom in enough that they are triangles
- Composition think of separate number lines and how nudging one number line changes the others (chain rule)

## Exponential
Derivative of exponential is a factor of itself. Is there a base where that factor is 1. The answer is `e^x`. Exploit the fact that all exponentials can be expressed in term of `e^x`.

## Implicit Differentiation
View each side of the equation as a two-variable function, f(x, y).

## Limits
When the limit exists you can make the output range (epsilon) as small as you want, but when the limit doesn't exist that output range can't get smaller than some particular values no matter how much you shrink the input range (delta) around the limiting input. L'Hospital rule uses derivative of the same function to figure out limit around an undefined point:
```
lim f(x)/g(x) = (df/dx)(a)dx/ (dg/dx)(a)(dx)
x->a
```

# Integral
Inverse of derivative.

## Fundamental Theorem of Calculus
```
b
S f(x) dx = F(b) - F(a)
a

(dF/dx)(x) = f(x)
```

# Taylor Series
Approximate actual output information near a point by using derivative information (higher order derivative) at that point.
```
P(x) = f(a) + (df/dx)(a)(((x-a)^1)/1!) + (d^2f/dx^2)(a)(((x-a)^2)/2!) + ...
```

# Resources:
- 3blue1brown -- Essence of calculus
