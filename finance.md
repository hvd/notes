# Where to invest?
1. Low cost Index ETFs -- Best place
2. Mutual funds -- Watch out for fees
3. Individual stocks -- High risk

# Retirement investment options
Generally, you’ll want to invest in the following order:
1. Put $1 in a Roth IRA asap (via backdoor)
This’ll start your 5-year window for penalty free Roth IRA earnings withdrawals. The earlier you start this, the more freedom you’ll have later on. Many people won’t be able to do this, so they’ll need to do this via the backdoor strategy.
2. Max out 401k.
3. Max out Backdoor Roth IRA.
4. Max out Mega Backdoor Roth.

## 401k
max-401k-limit = regular-401k-contribution-max + employer-contribution + after-tax-contribution

## Traditional IRA
Because of 401k and big salary, can’t deduct from our taxes. Technically this can be up to Apr 15 of the following year. For example, for 2017, you must contribute to your IRA between Jan 1, 2017, and Apr 15, 2018. If you contribute between Jan 1, 2018, and Apr 15, 2018, you can deduct it from either your 2017 or 2018 taxes. Be mindful that limits change, so make sure not to exceed those depending on which year you apply it to. For example, you could deduct $6,000 in 2019, but only $5,500 in 2018.

Caveats:
- If you expect to earn more money in the future, and eventually want to do a backdoor Roth IRA, having traditional IRA accounts will complicate the process and/or require you to pay taxes.

## Roth IRA
Ability to withdraw contributions at any time, and earnings after 5 years of account funding.

Caveats:
- If you only have a finite set of income to invest, and you believe your taxes right now are going to be higher than in the future, you may be better off investing in 401k pre-tax instead of putting money in a Roth IRA.
- The total contribution limit is across all IRAs including traditional.

## Backdoor Roth IRA
Contribute to Roth IRA, despite making too much money to contribute to a Roth IRA directly.

How to do it:
1. Contribute to a traditional IRA.
2. Make sure to not deduct this on your taxes.
3. Immediately after the funds are transferred, convert it to a Roth IRA.

Caveats:
- If you have any traditional IRAs, you should dismantle those first, or use the mega backdoor Roth instead.
Otherwise, when you convert from traditional IRA to Roth IRA, you’ll be taxed on the full amount of money in all your traditional IRAs. This is called the pro-rata rule. You can dismantle them by rolling them over to the current employer’s 401k.
- If you don’t convert the funds immediately, you run the risk of paying taxes on gains.
- By converting funds immediately, this technically violates the IRS’s "step transaction doctrine" rule, but it’s probably of little concern.
- Each conversion may reset the 5-year window.
- Rolling over more than one IRA per year may land you in trouble because of IRA One-Rollover-Per-Year Rule.

## Mega backdoor Roth
Contribute up to maximum allowance to a Roth 401k, or Roth IRA, beyond max allowance. This ends up being tens of thousands of dollars. To get the actual number do max-401k-limit - regular-401k-contribution-max - employer-match.

Prerequisites:
- You should be contributing the full allowance to your 401k.
If you aren’t yet contributing the full amount, you might as well contribute to Roth 401k directly and avoid the hassles of the mega backdoor.
- You probably want to contribute the full allowance via the backdoor Roth IRA.
If your total backdoor and mega backdoor contributions are less than IRA max total contributions, you might as well max out the vanilla backdoor first as it gives you more flexibility.

How to do it:
1. Before doing anything, set up automatic after-tax 401k to Roth 401k conversion. You want to do this before the after-tax actually hits your account. Otherwise, you’ll complicate the process a bit and may earn gains between when the after-tax money is posted to your account and when the conversion actually takes place, complicating your taxes.
2. Change your 401k contributions to include after-tax contributions.
3. Once the funds are transferred, make sure to invest them.

Caveats:
- If you don’t do the conversion immediately, you run the risk of earning gains and complicating your taxes.
- Some people think it’s a gray area in terms of legality according to tax law.
- You could convert after-tax to Roth IRA: either directly after tax to Roth IRA, or indirectly after tax to Roth 401k to Roth IRA.
- Some recommend converting your pre-tax and Roth balances to a traditional IRA. While this increases your investment options, it also complicates the backdoor Roth if you ever want to do it in the future. Thus, you may not want to follow that advice.
- You are responsible for stopping after-tax contributions once the cap (max-401k-limit - regular-401k-contribution-max - employer-contribution) is hit.
- If you contribute too much to after-tax, you may end up not being able to contribute the full 401k max salary deferral. For example, if the after-tax limit is 50k, and you make 200k.

## True up
Front load 401K and still get full employer match.

# Health saving account (HSA)
- If you’re going the full investment route, make sure you have money saved for medical expenses.
- Some states have their own regulations. Notably, HSA contributions are taxed by California.
- It costs $2,000 to play this game, which may not make sense for smaller amounts.
- The cost of medical expenses will be higher on this plan, so sign up at your own risk.

# ESPP
## Qualifying disposition
If sell ESPP after 2 years after the offering begin date and 1 year after the purchase date. Cost basis is based on the offering begin date and gains other than the discount will be taxed as long-term capital gains.

### Example
- Offering Begin Date: 12/10/2020
- FMV (full market value) on Offering Begin Date: $68
- Purchase Date: 05/15/2021
- FMV on Purchase Date: $141.2
- Purchase Price: $57.8 after 15% discount
- Sell at $151.2

Then:
- Disqualified ST: 5/15/21 <= sell date <= 12/10/21, Ordinary income tax on 141.2 - 57.8, ST capital gain tax on 151.2 - 141.2
- Disqualified LT: 12/11/21 <= sell date <= 12/10/22, Ordinary income tax on 141.2 - 57.8, LT capital gain tax on 151.2 - 141.2
- Qualified LT: sell date <= 12/11/22, Ordinary income tax on 68 - 57.8, LT capital gain tax on 151.2 - 68

# Wash sale
Occurs when one re-acquires a substantially similar asset within 30 days before or after of a sale that incurs a capital loss. In a wash sale, the capital loss can't be realized and must be added to the cost basis of the reacquired shares.

## Example
### No wash sale
- Start: 100 shares of ABC held for over a year, purchased for $25/share for $2500 cost basis
- Jan 1 sell 100 shares @ $20/share: $2000 cash, $500 capital loss
- Mar 1 buy 100 shares @ $20/share: $0 cash, $500 capital loss

### Wash sale, stable stock price
- Start: 100 shares of ABC held for over a year, purchased for $25/share for $2500 cost basis
- Jan 1 sell 100 shares @ $20/share: $2000 cash, $500 capital loss
- Jan 10 buy 100 shares @ $20/share: $0 cash, no capital loss, $2500 cost basis
- Mar 1 sell 100 shares @ $20/share: $2000 cash, $500 capital loss

# Tax
- Make sure and check RSU sales (cost basis, type of capital gain) are correct.
- ESPP income should be reported in Box 1 of your W-2. The company might disclose this amount in Box 14, but this isn't required. Cost basis might not be reported correctly from the sales proceeds on the Form 1099-B you get from your broker either. If it's not shown in Box 14, use information on Form 3922 from employer and information in supplemental materials that come with 1099-B to figure it out and use Form 8949.
- https://tomo-otsuka.github.io/brasstax/#/brasstax/estimated-taxes
